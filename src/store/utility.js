export const updateObject = (oldObject, updatedProperties) => {
    return {
      ...oldObject,
      ...updatedProperties,
    };
  };
  
  export const updateObjectInArrayBasedOnId = (
    oldObjectArray,
    changedObject,
    key
  ) => {
    let newState = {
      ...oldObjectArray,
    };
  
    newState[key] = newState[key].map((item, index) => {
      if (changedObject._id === item._id) {
        return changedObject;
      } else {
        return item;
      }
    });
  
    return newState;
  };
  
  export const deleteObjectInArrayBasedOnId = (
    oldObjectArray,
    changedObject,
    key
  ) => {
    let newState = {
      ...oldObjectArray,
    };
  
    newState[key] = newState[key].filter((item) => item._id !== changedObject._id);
  
    return newState;
  };
  