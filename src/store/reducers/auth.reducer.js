import * as actionType from '../actions/actionTypes';
import { updateObject } from "store/utility";
const initialState = {};

export const updateSignup = (state, action) => {
  return updateObject(state, {
    signup: action.payload,
  });
};

export const updateLogin = (state, action) => {
  return updateObject(state, {
    login: action.payload,
  });
};

export const updateVerifyUser = (state, action) => {
  return updateObject(state, { auth: action.payload });
};

const reducer = (state = initialState, action) => {

  switch (action.type) {
    case actionType.DO_SIGNUP:
      return updateSignup(state, action);

    case actionType.DO_SIGNIN:
      return updateLogin(state, action);

    case actionType.DO_VERIFY_USER:
      console.log("verify user")
      return updateVerifyUser(state, action);

    case actionType.DO_LOGOUT:
      return initialState;

    default:
      return state;
  }
};

export default reducer;
