import * as actionTypes from './actionTypes';

export const apiRequest = (method, url, body, history) =>{
    url = `${process.env.REACT_APP_API_URL}/${url}`;
    return{
        type : actionTypes.API_REQUEST,
        payload: body,
        meta: {method, url},
        history : history
    };
};

export const authRequest = (method, url, headers) =>{
    url = `${process.env.REACT_APP_API_URL}/${url}`;
    return{
        type : actionTypes.AUTH_REQUEST,
        meta: {method, url, headers},
    };
};
