export const API_REQUEST = 'API_REQUEST';
export const AUTH_REQUEST = 'AUTH_REQUEST';

export const DO_SIGNIN = 'SIGNIN';
export const DO_SIGNIN_SUCCESS = 'DO_SIGNIN_SUCCESS';
export const DO_SIGNIN_FAIL = 'DO_SIGNIN_FAIL';

export const DO_SIGNUP = 'DO_SIGNUP';
export const DO_SIGNUP_SUCCESS = 'DO_SIGNUP_SUCCESS';
export const DO_SIGNUP_FAIL = 'DO_SIGNUP_FAIL';

export const DO_VERIFY_USER = "DO_VERIFY_USER";
export const DO_VERIFY_SUCCESS = "DO_VERIFY_SUCCESS";
export const DO_VERIFY_FAIL = "DO_VERIFY_FAIL";

export const DO_LOGOUT = 'LOGOUT';


/** DATA **/
export const CREATE_STUDENT = "CREATE_STUDENT";
export const GET_STUDENT_LIST = "GET_STUDENT_LIST";
export const EDIT_STUDENT = "EDIT_STUDENT";
export const DELETE_STUDENT = "DELETE_STUDENT";


