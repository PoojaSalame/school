import * as actionTypes from './actionTypes';

export const doSignup = (data, history) => {
    return {
        type: actionTypes.DO_SIGNUP,
        payload: data,
        meta: {
            method: "POST",
            url: `user/signup`,
        },
        history: history,
    };
};

export const doLogin = (data, history) => {
    return {
        type: actionTypes.DO_SIGNIN,
        payload: data,
        meta: {
            method: "POST",
            url: `user/login`,
        },
        history: history,
    };
};


export const doVerifyUser = () => {
    return {
        type: actionTypes.DO_VERIFY_USER,
        meta: {
            method: "GET",
            url: `auth/verifyUser`,
            headers: {
                "x-access-token" : localStorage.getItem("token")
            }
        }
    };
};

export const doLogout = () => {
    return {
        type: actionTypes.DO_LOGOUT
    }
}