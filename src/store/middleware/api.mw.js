import {
    API_REQUEST, AUTH_REQUEST
} from "../actions/actionTypes";
import axios from "axios";

export const apiRequest = ({ dispatch }) => (next) => (action) => {
    if (action.type === API_REQUEST) {

        const { method, url } = action.meta;
        const URL = url;
        console.log("action.data3", action);

        //axios.defaults.withCredentials = true;
        axios(URL, {
            method: method,
            data: action.payload,
            //withCredentials: true,
        }).then((response) => {
            dispatch({ type: response.data.type, payload: response.data, history: action.history });
        }).catch((error) => {
            dispatch({ type: error.response.data.type, payload: error });
        });
    }
    return next(action);
};

export const authRequest = ({ dispatch }) => (next) => (action) => {
    if (action.type === AUTH_REQUEST) {

        const { method, url, headers } = action.meta;
        const URL = url;
            //axios.defaults.withCredentials = true;
        axios(URL, {
            method: method,
            headers:headers
            //withCredentials: true,
        }).then((response) => {
            console.log("auth response :: ", response.data);
            dispatch({ type: response.data.type, payload: response.data});
        }).catch((error) => {
            console.log("auth response error : ", error.response.data.type);
            dispatch({ type: error.response.data.type, payload: error });
        });
    }
    return next(action);
};

export const apiMiddleware = [apiRequest, authRequest];