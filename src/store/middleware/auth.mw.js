import * as actionTypes from '../actions/actionTypes';
import *as actions from "../actions";

export const doSignup = ({ dispatch }) => (next) => (action) => {
  if (action.type === actionTypes.DO_SIGNUP) {

    const { method, url } = action.meta;
    dispatch(
      actions.apiRequest(method, url, action.payload, action.history)
    );
  }
  if (action.type === actionTypes.DO_SIGNUP_SUCCESS) {

    action.history.push('/auth/login');
  }

  if (action.type === actionTypes.DO_SIGNUP_FAIL) {
    console.log("DO_SIGNUP_FAIL12: ", action);
  }

  next(action);
};


export const doLogin = ({ dispatch }) => (next) => (action) => {
  if (action.type === actionTypes.DO_SIGNIN) {

    const { method, url } = action.meta;
    dispatch(
      actions.apiRequest(method, url, action.payload, action.history)
    );
  }

  if (action.type === actionTypes.DO_SIGNIN_SUCCESS) {
    localStorage.setItem("token", action.payload.data.token)
    console.log("state : action.payload : ",action.payload )
    action.history.push({pathname :'/admin/index', state : action.payload });
  }

  if (action.type === actionTypes.DO_SIGNIN_FAIL) {
    console.log("DO_SIGNIN_FAIL12: ", action);
  }

  next(action);
};

export const doVerifyUser = ({ dispatch }) => (next) => (action) => {
  if (action.type === actionTypes.DO_VERIFY_USER) {

    const { method, url, headers } = action.meta;
    
    dispatch(
      actions.authRequest(method, url, headers)
    );
  }
  console.log("after if verify: ", action);

  if (action.type === actionTypes.DO_VERIFY_SUCCESS) {
    console.log("actionTypes.DO_VERIFY_SUCCESS: ", action);
  }

  if (action.type === actionTypes.DO_VERIFY_FAIL) {
    console.log("DO_VERIFY_FAIL: ", action);
  }
  next(action);
};

export const authMiddleware = [
  doSignup,
  doLogin,
  doVerifyUser
]