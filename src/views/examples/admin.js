import React from 'react'
import { Card, CardBody, CardTitle, Container, Row, Col } from "reactstrap";


const admin = () => {
    return (
        <Container fluid="md" className="p-3 mb-10 ">
            <Card className="card-stats mb-4 mb-xl-0 border border-primary">
                <CardBody>
                    <Row>
                        <div className="col">
                            <CardTitle
                                tag="h5"
                                className="text-uppercase text-muted mb-0"
                            >
                                ADMIN VIEW
                        </CardTitle>
                            <span className="h2 font-weight-bold mb-0">VIEW TEACHER AND STUDENT</span>
                        </div>
                    </Row>
                </CardBody>
            </Card>
        </Container>
    )
}

export default admin
