/*!

=========================================================
* Argon Dashboard React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import * as actions from '../store/actions';
// node.js library that concatenates classes (strings)
import classnames from "classnames";
// javascipt plugin for creating charts
import Chart from "chart.js";
// react plugin used to create charts
import { Line, Bar } from "react-chartjs-2";
// reactstrap components
import { Card, CardBody, CardTitle, Container, Row, Col } from "reactstrap";


// core components


import Header from "components/Headers/Header.js";

const Index = (props) => {

  const [isLoggedIn, setIsLoggedIn] = useState(false);
  //const [userData, setUserData] = useState([]);
  const [userRole, setUserRole] = useState([]);
  //const [result, setResult] = useState([]);

  useEffect(() => {

    const user = props.location.state;
    
    //setResult(user.result);

    const userRole = props.location.state.data.user;

    setUserRole(userRole.role[0]);

    if (user.result === true) {
      console.log("check user role : ", userRole);
      props.doVerifyUser();
      console.log("props : ", props)

      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
    }

  }, [])

  return (
    <>
      <Header />
      {/* Page content */}
      {isLoggedIn && (<Container className="mt--7" fluid>
        <Row>
        
          {userRole === "ROLE_ADMIN" && (<div>
            <div fluid="md" className=" container p-3 mb-10 ">
              <Card className="card-stats mb-4 mb-xl-0 border border-primary">
                <CardBody>
                  <Row>
                    <div className="col">
                      <CardTitle
                        tag="h5"
                        className="text-uppercase text-muted mb-0"
                      >
                        ADMIN VIEW
                    </CardTitle>
                      <span className="h2 font-weight-bold mb-0">VIEW TEACHER AND STUDENT</span>
                    </div>
                  </Row>
                </CardBody>
              </Card>
            </div>
          </div>)}
          

          {(userRole === "ROLE_TEACHER" || userRole === "ROLE_ADMIN") && (<div>
            <div fluid="md" className=" container p-3 mb-10 ">
              <Card className="card-stats mb-4 mb-xl-0 border border-primary">
                <CardBody>
                  <Row>
                    <div className="col">
                      <CardTitle
                        tag="h5"
                        className="text-uppercase text-muted mb-0"
                      >
                        TEACHER VIEW
                </CardTitle>
                      <span className="h2 font-weight-bold mb-0">VIEW OWN AND STUDENT PROFILE ONLY</span>
                    </div>
                  </Row>
                </CardBody>
              </Card>
            </div>
          </div> )}
          
          {(userRole === "ROLE_USER" || userRole === "ROLE_ADMIN" || userRole === "ROLE_TEACHER") && (<div>
            <div fluid="md" className=" container p-3 mb-10 ">
              <Card className="card-stats mb-4 mb-xl-0 border border-primary">
                <CardBody>
                  <Row>
                    <div className="col">
                      <CardTitle
                        tag="h5"
                        className="text-uppercase text-muted mb-0"
                      >
                        STUDENT VIEW
                </CardTitle>
                      <span className="h2 font-weight-bold mb-0">STUDENT PROFILE ONLY</span>
                    </div>
                  </Row>
                </CardBody>
              </Card>
            </div>
          </div>)}

        </Row>
      </Container>)}
    </>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    doVerifyUser: () => dispatch(actions.doVerifyUser())
  };
};

export default connect(null, mapDispatchToProps)(Index);
